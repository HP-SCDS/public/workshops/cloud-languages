import React from 'react';
import HeaderComponent from './components/header/header';
import ContentComponent from './components/content/content';

import './App.scss';

const App: React.FC = () => {
  return (
    <div className="app-component">
      <HeaderComponent></HeaderComponent>
      <ContentComponent></ContentComponent>
    </div>
  );
}

export default App;
