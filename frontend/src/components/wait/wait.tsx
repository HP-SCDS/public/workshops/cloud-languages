import React, { FunctionComponent } from 'react';
import { Spinner } from 'react-bootstrap';

type WaitComponentProps = {}
export const WaitComponent: FunctionComponent<WaitComponentProps> = () => (
    <Spinner animation="grow" variant="primary" role="status">
        <span className="sr-only">Loading...</span>
    </Spinner>
);
