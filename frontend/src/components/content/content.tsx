import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import { WaitComponent  } from '../wait/wait';

import './content.scss';

export class ContentComponent extends Component {
    render() {
        return <Container className="content-component">
            <WaitComponent></WaitComponent>
        </Container>
    }
}

export default ContentComponent;