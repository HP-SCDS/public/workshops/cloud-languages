package controllers

import (
	"encoding/json"

	"lotrquiz/api/models"
	"lotrquiz/quiz"
	"net/http"
)

// QuestionsHandler manages quetions resources
func QuestionsHandler(q *quiz.Quiz) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			getQuestions(q, w)
			return
		default:
			w.WriteHeader(http.StatusNotFound)
		}
	}
}

// QuestionHandler manages quetion
func QuestionHandler(q *quiz.Quiz) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodGet:
			getQuestion(q, w)
			return
		default:
			w.WriteHeader(http.StatusNotFound)
		}
	}
}

func getQuestion(q *quiz.Quiz, w http.ResponseWriter) {
	question := q.GetQuestion()
	response := &models.Question{
		ID:          &question.ID,
		Description: &question.Description,
	}
	answ := make([]*models.Answer, len(question.Answers))
	for j, answer := range question.Answers {
		answ[j] = &models.Answer{
			ID:       &answer.ID,
			Solution: &answer.Solution,
		}
	}
	response.Answers = answ
	json.NewEncoder(w).Encode(response)
}

func getQuestions(q *quiz.Quiz, w http.ResponseWriter) {
	questions := q.GetQuestions()

	response := make([]*models.Question, len(questions))

	for i, question := range questions {
		ques := &models.Question{
			ID:          &question.ID,
			Description: &question.Description,
		}
		answ := make([]*models.Answer, len(question.Answers))
		for j, answer := range question.Answers {
			answ[j] = &models.Answer{
				ID:       &answer.ID,
				Solution: &answer.Solution,
			}
		}
		ques.Answers = answ
		response[i] = ques
	}
	json.NewEncoder(w).Encode(response)
}
