package controllers

import (
	"encoding/json"

	"lotrquiz/quiz"
	"net/http"
)

// ValidateAnswerHandler returns an endpoint handler
func ValidateAnswerHandler(q *quiz.Quiz) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case http.MethodPost:
			validateAnswer(q, w, r)
			return
		default:
			validateAnswer(q, w, r)
		}
	}
}

func validateAnswer(q *quiz.Quiz, w http.ResponseWriter, r *http.Request) {
	var answer struct {
		QuestionID string
		AnswerID   string
	}

	err := json.NewDecoder(r.Body).Decode(&answer)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	isValid := q.IsValidAnswer(answer.QuestionID, answer.AnswerID)
	if !isValid {
		http.Error(w, "Invalid response", http.StatusBadRequest)
		return
	}
	w.WriteHeader(http.StatusOK)
}
