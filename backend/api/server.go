package api

import (
	"net/http"

	"lotrquiz/api/controllers"
	"lotrquiz/quiz"
)

// Config is a server configuration
type Config struct {
	Address string
	Quiz    *quiz.Quiz
}

// Server defines a server configuration
type Server struct {
	address string
	quiz    *quiz.Quiz
}

// NewServer creates a new server instance
func NewServer(config Config) *Server {
	return &Server{address: config.Address,
		quiz: config.Quiz}
}

// Listen runs http server
func (s *Server) Listen() {
	s.setupRouter()
	http.ListenAndServe(s.address, nil)
}

func (s *Server) setupRouter() {
	http.Handle("/api/v1/questions", controllers.QuestionsHandler(s.quiz))
	http.Handle("/api/v1/question", controllers.QuestionHandler(s.quiz))
	http.Handle("/api/v1/answer/validate", controllers.ValidateAnswerHandler(s.quiz))
}
