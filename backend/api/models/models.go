package models

// Question defines a question
type Question struct {
	ID          *string   `json:"id,omitempty"`
	Description *string   `json:"description,omitempty"`
	Answers     []*Answer `json:"answers,omitempty"`
}

// Answer represents a question answer
type Answer struct {
	ID       *string `json:"id,omitempty"`
	Solution *string `json:"solution,omitempty"`
}
